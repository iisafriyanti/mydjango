from django.contrib import admin

# Register your models here.
from .models import Project, Person

admin.site.register(Project)
admin.site.register(Person)