from django import forms

class Add_Project_Form(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    pos_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Your position'
    }
    title_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Title of the project'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Description of the project'
    }

    position = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=pos_attrs))
    title = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))